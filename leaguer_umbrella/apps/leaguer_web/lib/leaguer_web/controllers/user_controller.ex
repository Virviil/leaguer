defmodule LeaguerWeb.UserController do
  use LeaguerWeb, :controller


  alias Leaguer.Accounts

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_status(:created)
        |> render("show.json", user: user)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(LeaguerWeb.ChangesetView, "error.json", changeset: changeset)
    end
  end
end
