defmodule LeaguerWeb.PageController do
  use LeaguerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
