defmodule LeaguerWeb.UserControllerTest do
  use LeaguerWeb.ConnCase

  alias Leaguer.Accounts.User
  alias Leaguer.Repo

  @valid_attrs %{email: "foo@bar.com", password: "password"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "creates and returns user when data is valid", %{conn: conn} do
    conn = post(conn, Routes.user_path(conn, :create), user: @valid_attrs)
    body = json_response(conn, 201)
    assert body["data"]["id"]
    assert body["data"]["email"]
    refute body["data"]["password"]
    assert Repo.get_by(User, email: @valid_attrs[:email])
  end
end
