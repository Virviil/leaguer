defmodule Leaguer.Repo do
  use Ecto.Repo,
    otp_app: :leaguer,
    adapter: Ecto.Adapters.Postgres
end
