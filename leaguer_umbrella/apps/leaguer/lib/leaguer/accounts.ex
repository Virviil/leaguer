defmodule Leaguer.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Leaguer.Repo

  alias Leaguer.Accounts.User

  @doc """
  Returns the list of accounts_User.

  ## Examples

      iex> list_accounts_User()
      [%User{}, ...]

  """
  def list_accounts_User do
    Repo.all(User)
  end

  @doc """
  Gets a single User.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_User!(123)
      %User{}

      iex> get_User!(456)
      ** (Ecto.NoResultsError)

  """
  def get_User!(id), do: Repo.get!(User, id)

  @doc """
  Creates a User.

  ## Examples

      iex> create_User(%{field: value})
      {:ok, %User{}}

      iex> create_User(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a User.

  ## Examples

      iex> update_User(User, %{field: new_value})
      {:ok, %User{}}

      iex> update_User(User, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = User, attrs) do
    User
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_User(User)
      {:ok, %User{}}

      iex> delete_User(User)
      {:error, %Ecto.Changeset{}}

  """
  def delete_User(%User{} = User) do
    Repo.delete(User)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking User changes.

  ## Examples

      iex> change_User(User)
      %Ecto.Changeset{source: %User{}}

  """
  def change_User(%User{} = User) do
    User.changeset(User, %{})
  end
end
