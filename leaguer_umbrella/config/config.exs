# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

# Configure Mix tasks and generators
config :leaguer,
  ecto_repos: [Leaguer.Repo]

config :leaguer_web,
  ecto_repos: [Leaguer.Repo],
  generators: [context_app: :leaguer]

# Configures the endpoint
config :leaguer_web, LeaguerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SZMn+S7tnr2BAYu0aK7vhy0jYN6CKiJy5fl3f8RXMyqs0tWi8Rm0C9vkhSRXz34I",
  render_errors: [view: LeaguerWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: LeaguerWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
